#test client
import socket

def client():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('10.89.63.209', 3232)) #IP is the server IP


	message = "this is a test from the python client"
	s.send(message.encode('utf-8'))

	rec = s.recv(4096)
	print(rec)
	s.close()

client()