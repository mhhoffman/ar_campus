# this file is for training the ML model to identify locations on campus
# 7/3/18: first locations to classify are the starbucks store front and the red sun dial sculpture
# code taken from https://gist.github.com/fchollet/0830affa1f7f19fd47b06d4cf89ed44d
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import optimizers
from keras import backend as K
import tensorflow as tf


# dimensions of our images.
img_width, img_height = 576, 768

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 70
nb_validation_samples = 20
epochs = 35
batch_size = 10

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

# linear model, adds layers one on top of another
model = Sequential()
#Convolution layer, detects features
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
#Activation layer, replaces features with value 0
model.add(Activation('relu'))
#reduces dimensionality of feature map
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

#initialize dropout layer to prevent overfitting
model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(2))
#converts data to probabilities for each class
model.add(Activation('sigmoid'))

# model = models.load_model("image_classification07.h5")

# rmsprop = optimizers.rmsprop(lr=0.00001)
# adam = optimizers.adam(lr = .000001)
sgd = optimizers.SGD(lr = .00005)

model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=  1./ 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode = 'categorical')
##    classes = ['starbucks', 'sun_dial'])

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=3,
    class_mode = 'categorical')
##    classes = ['starbucks', 'sun_dial'])

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // 3)


model.save('image_classification38.h5')
