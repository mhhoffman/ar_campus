# this file is for training the ML model to identify locations on campus
# 7/3/18: first locations to classify are the starbucks store front and the red sun dial sculpture
# code taken from https://gist.github.com/fchollet/0830affa1f7f19fd47b06d4cf89ed44d


from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K


# dimensions of our images.
img_width, img_height = 576, 768

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
nb_train_samples = 80
nb_validation_samples = 10
epochs = 10
batch_size = 16

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

MLmodel = Sequential()
MLmodel.add(Conv2D(32, (3, 3), input_shape=input_shape))
MLmodel.add(Activation('relu'))
MLmodel.add(MaxPooling2D(pool_size=(2, 2)))

MLmodel.add(Conv2D(32, (3, 3)))
MLmodel.add(Activation('relu'))
MLmodel.add(MaxPooling2D(pool_size=(2, 2)))

MLmodel.add(Conv2D(64, (3, 3)))
MLmodel.add(Activation('relu'))
MLmodel.add(MaxPooling2D(pool_size=(2, 2)))

MLmodel.add(Flatten())
MLmodel.add(Dense(64))
MLmodel.add(Activation('relu'))
MLmodel.add(Dropout(0.5))
MLmodel.add(Dense(1))
MLmodel.add(Activation('sigmoid'))

MLmodel.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary')

MLmodel.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size)

MLmodel.save_weights('first_try.h5')