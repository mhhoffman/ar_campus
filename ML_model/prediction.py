#file for predicting starbucks or sundial for new images
from keras.preprocessing import image
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
from keras import optimizers
import tensorflow as tf
import numpy as np
from keras import models
import cv2

#file_location = input("write file name")
starbucks_img1 = "starbucksTEST.jpg"
sun_dial_img1 = "sun_dialTEST.jpg"
cat_img = "noTEST.jpg"
starbucks_img2 = "starbucksTEST2.jpg"
sun_dial_img2 = "sun_dialTEST2.jpg"

images = [cat_img, starbucks_img1, starbucks_img2, sun_dial_img1, sun_dial_img2]

# dimensions of our images.
img_width, img_height = 576, 768

#load with CV library
##test_image = cv2.imread(file_location)
##test_image = np.expand_dims(test_image, axis=0)

#load image with keras
def load_image(filename):
	test_image = image.load_img(filename, target_size = (img_width, img_height))
	x = image.img_to_array(test_image)
	#normalize array output
	x *= (255.0/x.max())
	img = np.expand_dims(x, axis = 0)
	return img


model = models.load_model("image_classification37.h5")
# sgd = optimizers.SGD(lr=0.000025, momentum=0.0, decay=0.0, nesterov=False)
# rmsprop = optimizers.rmsprop(lr=0.001)
sgd = optimizers.sgd(lr = .000025)


model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])

# print("layers ")
# for layer in model.layers:
# 	print(layer)
# print("inputs ")
# for intensor in model.inputs:
# 	print(intensor)
# print("outputs ")
# for output in model.outputs:
# 	print(output)
# print("summary ")
# print(model.summary())

for img in images:
	print(model.predict(load_image(img)))

# print(model.predict(load_image("water_bottle.jpg")))

# result = model.predict(img)
# print(result)
#first number is starbucks indicator
#second number is sun_dial indicator
