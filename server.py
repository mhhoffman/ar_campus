#Server file for ar_Campus App
#using UDP protocol

from keras.preprocessing import image as kimage
from keras import models
from keras import optimizers
from keras import backend as K
import numpy as np

import socket
import sys

from PIL import Image
import io


def server():
	#initialize connection
	#create a TCP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	ip = '10.89.63.209'
	#bind the socket to the port
	server_address = (ip, 3000)
	print("starting up on %s port %s" % server_address)
	sock.bind(server_address)

	#load the keras model for image classification
	model = load_model()
	while True:

		#read from socket connection
		print('\nwaiting to receive a message')
		#buffer size is 1024
		data, address = sock.recvfrom(65535)

		print('recevied %s bytes from %s' % (len(data), address))

		# f = open("testing.jpg", "wb")
		# f.write(data)
		# f.close()

		# image = Image.open(io.BytesIO(data))
		# image.show()

		response = run_model(model, data)
		print(response)

		if data:
			sent = sock.sendto(response, address)
			print('sent %s bytes back to %s' %(sent, address))
		#run image through model
		#send classification back
		
#method to load and initialize the image classification keras model
def load_model():
	#load model
	model = models.load_model("image_classification36.h5")
	sgd = optimizers.sgd(lr = .000025)
	#compile model
	model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])
	return model

#method to run the model on the image, both objects are passed into the function
def run_model(model, image):
	return model.predict(load_image(image))


#method to preprocess the image
def load_image(imageData):
	img_height, img_width = 576, 768
	image = Image.open(io.BytesIO(imageData))
	image.show()
	#normalize array output
	resized = image.resize((img_width, img_height), Image.ANTIALIAS)
	resized.show()
	x = kimage.img_to_array(resized)
	temp = x * (255.0/x.max())
	img = np.expand_dims(temp, axis = 0)
	return img

server()