#server file using basic server socket
#code taken from https://gist.github.com/MinaGabriel/c97b7a22c40c94b283723c7226dc9b68
import socket

def server():
	ip = '10.89.63.209'
	port = 3232

	#create socket with ip address and two-way connection based byte stream
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print("socket created")

	#bind socket to host and port
	try:
		s.bind((ip, port))
	except socket.error as err:
		print("Bind Failed, Error Code: " + str(err[0]) + ", Message: " + err[1])
		sys.exit()

	print("Socket Bind Success!")

	#set up and start TCP listener
	s.listen(10)
	print("Socket is now listening")

	conn, addr = s.accept()
	print("Connect with " + addr[0] + ":" + str(addr[1]))
	buff = conn.recv(4096)
	print(buff)
	received_message = "Thank you for connecting"
	conn.send(received_message.encode('utf-8'))
	conn.close()

server()
