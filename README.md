# README #

### What is this repository for? ###

* This repository contains the Machine Learning model created for recognizing locations on the HKUST campus.
*	It also includes an android App that connects to a server which implement the model to recognize locations on HKUST Campus
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Created by Meital Hoffman meital@mit.edu
* Part of International Undergraduate Research Opportunities Program (IROP) at the Hong Kong University of Science and Technology (HKUST)
* Under the supervision of Professor Pan Hui and Tristan Braud PhD
* Project timeline: 6/18/18 - 8/8/18